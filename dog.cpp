///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @February 13 2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {

Dog::Dog( string newName, enum Color newColor, enum Gender newGender) {
   gender = newGender;

   species = "Doggy";

   hairColor = newColor;

   gestationPeriod = 63;

   name = newName;

}

const string Dog::speak() {
   return string ("Woof");
}

void Dog::printInfo(){
   cout << "Dog name - [" << name << "]" << endl;
   Mammal::printInfo();
}

}
