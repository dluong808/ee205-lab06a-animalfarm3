///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @February 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

//Constructor and Destructor definitions
Animal::Animal(){
   cout << ".";
}

Animal::~Animal(){
   cout << "x";
}
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	
string Animal::bool2string (bool input){
   switch (input){
      case 0: return "FALSE";
      case 1: return "TRUE";
   }
}


string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch(color){
      case BLACK:
         return string("BLACK");
      case WHITE:
         return string ("WHITE");
      case RED:
         return string ("RED");
      case SILVER:
         return string ("SILVER");
      case YELLOW:
         return string ("YELLOW");
      case BROWN:
         return string ("BROWN");
      default: return string("Unknown");  }
   }
	
} // namespace animalfarm
