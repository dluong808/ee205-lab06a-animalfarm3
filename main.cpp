///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @March 13 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <time.h>
#include <random>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

#define WEIGHT_MAX (200.00)
#define WEIGHT_MIN (20.00)
#define COLOR_MAX (6)
#define GENDER_MAX (2)
#define ANIMAL_TYPES (6)


using namespace std;
using namespace animalfarm;

// Random Generators
static const Gender getRandomGender()
{
   return (Gender) (rand() % GENDER_MAX);
};

static const Color getRandomColor()
{
   return (Color) (rand() % COLOR_MAX);
 };

static const bool getRandomBool(){

   if ( rand() % 2 == 0)
      return true;
   else return false;

 };

static const float getRandomWeight( const float min , const float max ){

   return ((max - min) * ((float)rand() / RAND_MAX)) + min;

};

static const char getRandomConsonent(){
  char consonents[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};
   return consonents[rand () % 20];
   };

static const char getRandomVowel(){
   char vowels[] = {'a','e','i','o','u','y'};
   return vowels[rand() % 6];
};

static const char getRandomChar(){
   int random = rand()% 2;
   if(random==0){
      return getRandomConsonent();}
   else{
      return getRandomVowel();
   }
};

static const string bool2string (bool input){
   switch (input){
      case 0: return "FALSE";
      case 1: return "TRUE";
   }
}


static const string getRandomName(){

      char consonents[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};

      int length = rand () % 5 + 4; //Random from 4 letters up to 9(4+5)
      char randomName[length];
      int random= rand()& 2;
      int consonentCheck=0;
      randomName[0]=toupper( getRandomChar() );

      for (int i=1;i<length;i++){

         for(int j=0;j<19;j++){
         if (randomName[i-1]==consonents[j]){
            consonentCheck=1;
         }
            if(consonentCheck==1){
            randomName[i]=getRandomVowel();
            }
            else{
               randomName[i]=getRandomChar();
            }
      }
   }
   return randomName;
 };


class AnimalFactory {
   public:
      static Animal* getRandomAnimal();
};

//Animal Factory class that generates an Animal value into a pointer of an animal

Animal* AnimalFactory::getRandomAnimal()
   {
   Animal* newAnimal = NULL;
   int i = rand() % ANIMAL_TYPES;
     switch (i)  {
         case 0: newAnimal = new Cat (getRandomName() , getRandomColor(), getRandomGender());
         break;

         case 1: newAnimal = new Dog ( getRandomName(), getRandomColor(), getRandomGender() );
         break;

         case 2: newAnimal = new Nunu ( getRandomBool() , RED ,getRandomGender());
         break;

         case 3: newAnimal = new Aku ( getRandomWeight(WEIGHT_MIN, WEIGHT_MAX) , SILVER, getRandomGender() );
         break;

         case 4: newAnimal = new Palila ( getRandomName() , YELLOW, getRandomGender() );
         break;

          case 5: newAnimal = new Nene (getRandomName(), BROWN, getRandomGender() );
         break;
         }
      return newAnimal;

   };

int main() {
//random number generators
srand (time(NULL));
string tOrF;

cout << "Welcome to Animal Farm 3" << endl;

//Initialize an Array container pointers to animal
array<Animal*, 30> animalArray;
animalArray.fill( NULL );
for (int i = 0 ; i < 25 ; i++ ) {
animalArray[i] = AnimalFactory::getRandomAnimal();
}

cout << endl << "Array of Animals" << endl;
cout << "   Is it empty: " << bool2string(animalArray.empty() ) << endl ;
cout << "   Number of elements: " << animalArray.size() << endl;
cout << "   Max size : " << animalArray.max_size() << endl;
//Iterate over the Array calling the speak function each time

// 
for( Animal* animal : animalArray  )  {
   
   //Only calls speak function if there is an animal there not NULL
   if ( animal != NULL){
 cout << animal->speak() << endl;
   }
}

//Deletes animal
for (int i=0; i < animalArray.size();i++)
{
   delete animalArray[i];
}

// Test is still empty
//cout << "   Is it empty: " << animalArray.empty()  << endl ;


list<Animal*> animalList;
for (int i = 0 ; i < 25 ; i++ ) {
 animalList.push_front(AnimalFactory::getRandomAnimal());
}

cout << endl << "List of Animals" << endl;
cout << "   Is it empty "<< bool2string(animalList.empty() ) << endl;
cout << "   Number of elements: " << animalList.size() << endl;
cout << "   Max size: " << animalList.max_size() << endl;

for (Animal* animal : animalList){
   cout << animal->speak() << endl;
}
 
for (Animal* animal : animalList){
   delete animal;
}
//Test if empty after
//cout << "   Is it empty "<< animalList.empty() << endl;


}
