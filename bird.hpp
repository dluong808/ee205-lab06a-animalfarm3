///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @February 13 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Bird : public Animal {
public:
   enum Color featherColor;
   // bool and strings for migratory
   bool isMigratory;
   string migratory;
   //Most birds makes the same sound
   virtual const string speak();

   void printInfo();
};

} // namespace animalfarm

